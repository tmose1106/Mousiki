# Standard
import os
# Dependency
import setuptools

setuptools.setup(
    name="mousiki",
    description="An audio library suite written in Python 3",
    version="0.1.0",
    author='Ted Moseley',
    author_email='tmoseley1106@gmail.com',
    url='https://gitlab.com/tmose1106/mousiki',
    packages=[
        "mousiki",
        "mousiki/cli",
        "mousiki/metadata"
    ],
    entry_points={
        'console_scripts': [
            "mousiki=mousiki.cli:main",
        ],
    },
)
