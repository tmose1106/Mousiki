**************************************************
Íchou - *A suite of audio tools written in Python*
**************************************************

Íchou is a minimalist and modular suite of audio tools written in Python 3.

Included Tools
==============

* An audio CD ripper that retrieves metadata via Musicbrainz
* An audio filetype converter for lossless audio types
* A script for batch renaming files based upon their metadata
* Several scripts for analyzing music libraries

Dependencies
============

* Python 3+
* `click`_ (command line parsing) \*
* `Mutagen`_ (read and edit audio tags) \*
* `pyxdg`_ (finding the configuaration file) \*
* `setuptools`_ (installation)
* `toml`_ (human-readable configuration) \*

Optional Dependencies
---------------------

Íchou is a highly modular system of tools, thus it allows the following
optional dependencies.

* `cdparanoia`_ (ripping audio from compact discs)
* `discid`_ (finding CD metadata)
* `docutils`_ (building documentation and manpages) \*
* `ffmpeg`_ (converting audio formats)
* `musicbrainzngs`_ (finding CD metadata) \*
* `pillow`_ (handling album artwork files) \*

.. note::

	All dependencies that are followed by an asterisk are Python-based
	dependencies which can be installed using Pip and the included
	``requirements.txt`` file.

Developement Dependencies
-------------------------

* `flake8`_
* `pytest`_

.. note::

	The developement dependencies are not required, but are highly recommended.
	They can be installed using Pip and the included `requirements-dev.txt` file.

Installation
============



Licensing
=========

Copyright © 2016-2018 Ted Moseley. Free use of this software is granted under
the terms of the `MIT Open Source License`_. View ``LICENSE.txt`` for more
information.

.. _`cdparanoia`: https://xiph.org/paranoia/
.. _`click`: http://click.pocoo.org/5/
.. _`discid`: https://github.com/JonnyJD/python-discid
.. _`docutils`: http://docutils.sourceforge.net/
.. _`ffmpeg`: https://ffmpeg.org/
.. _`flake8`: https://gitlab.com/pycqa/flake8
.. _`musicbrainzngs`: https://github.com/alastair/python-musicbrainzngs
.. _`Mutagen`: https://mutagen.readthedocs.io/en/latest/
.. _`MIT Open Source License`: https://opensource.org/licenses/MIT
.. _`pillow`: https://python-pillow.org/
.. _`pytest`: https://docs.pytest.org/en/latest/
.. _`pyxdg`: https://www.freedesktop.org/wiki/Software/pyxdg/
.. _`setuptools`: https://setuptools.readthedocs.io/en/latest/
.. _`toml`: https://github.com/uiri/toml
