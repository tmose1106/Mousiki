*************
Configuration
*************

Configuration of mousiki takes place within a single ``config.toml`` file within
the default configuation sections for your operating system.

Value Types
===========

When specifying values within the configuration file, one can use the
following value types:

=================  ================================================  ==============================
Type Name          Type Description                                  Example
=================  ================================================  ==============================
integer[min, max]  A basic integer within min and max (inclusive)    3
metadata string    A string which may contain metadata keys          "{track_number}-{track_title}"
path string        A string which may contain environment variables  "$HOME/Music"
=================  ================================================  ==============================

.. note::

  Metadata keys can be viewed in ``naming_schemes.rst``.

General Configuration
=====================

General configuarion contains values which can be overwritten by sub-commands.
Possible values include:

===============  ===============  ================================================
Field Name       Value Type       Value Description
===============  ===============  ================================================
album_art_dir    path string      Where local album art should be searched for
filename_scheme  metadata string  How to name the output files
output_dir       path string      Where to set the root output directory for files
===============  ===============  ================================================

Encoders
========

Several commands handle audio files using encoders. The type of encoder is
defined by the codec which is specified.

flac
----

===============  ===============  ================================================
Field Name       Value Type       Value Description
===============  ===============  ================================================
compression      integer[0,12]    The compression level to be used
output_dir       path string      The output directory for this encoder's files
===============  ===============  ================================================

mp3
---

===============  ===============  ================================================
Field Name       Value Type       Value Description
===============  ===============  ================================================
bitrate          integer[4,320]   The bitrate value
output_dir       path string      The output directory for this encoder's files
===============  ===============  ================================================


*cd* Command
============

These configuration options are for the *cd* command!

*rip* Command
-------------
