*******
Testing
*******

Test Files
==========

This directory includes several different test files:

* Several formats of test files titled ``audio.<ext>``, all of which play **De
  Camptown Races** and originate from the MIDI file on its `Wikipedia page <https://en.wikipedia.org/wiki/Camptown_Races>`_.
  The original WAV file was created using `Fluidsynth <http://www.fluidsynth.org/>`_.
  The others were converted to their respective formats using `FFmpeg <http://ffmpeg.org/>`_.
* An album art file titled ``folder.jpg`` is a picture of a horse found
  on `Flickr <https://www.flickr.com/photos/internetarchivebookimages/20226120179>`_.

Running Tests
=============

mousiki uses `pytest <https://docs.pytest.org/en/latest/>`_ as its testing
framework. In order to run the full test suite, simply run
``python3 -m pytest`` from within the *test* directory.
