# Standard
import collections


def compare_dictionaries(dict_1, dict_2):
    """ Compare *dict_1* versus *dict_2*. """
    for dict_1_key, dict_1_value in dict_1.items():
        if dict_1_key not in dict_2:
            return False
        elif isinstance(dict_1_value, collections.Mapping):
            if not isinstance(dict_2[dict_1_key], collections.Mapping):
                return False
            elif not compare_dictionaries(dict_1[dict_1_key], dict_2[dict_1_key]):
                return False
        else:
            if dict_1[dict_1_key] != dict_2[dict_1_key]:
                return False

    return True
