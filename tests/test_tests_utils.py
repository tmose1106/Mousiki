# Testing
import utils


def test_compare_dictionaries_success():
    test_dict = {
        'Name': "Tad",
        'Age': 18,
        'Hobbies': {
            'Running': True,
        },
    }

    assert utils.compare_dictionaries(test_dict, test_dict)


def test_compare_dictionaries_fail():
    test_dict_1 = {
        'Name': "Tad",
        'Age': 18,
        'Hobbies': {
            'Running': False,
        },
        'Favorite_Color': "Red",
    }

    test_dict_2 = {
        'Name': "Tad",
        'Age': 18,
        'Hobbies': {
            'Running': True,
        },
        'Favorite_Color': "Red",
    }

    assert not utils.compare_dictionaries(test_dict_1, test_dict_2)
