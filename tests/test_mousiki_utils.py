"""
Tests for the mousiki.util module.
"""

# Dependency
import pytest
# Project
import mousiki.config
import mousiki.util
# Testing
import utils

# A sample configuration dictionary
SAMPLE_CONFIGURATION = {
    "output_dir": "/home/tmose1106/Music",
    "file_name_scheme": "{track_number}-{track_title}",
    "convert": {
        "output_dir": "Convert",
        "encoder": [
            {
                "codec": "flac",
                "compression_level": 8,
                "output_dir": "FLAC/{album_artist}/{album_title}",
            },
            {
                "codec": "mp3",
                "bitrate": 320,
                "compression_level": 3,
                "output_dir": "MP3_320/{album_artist}/{album_title}",
            },
        ],
    },
}

# A sample metadata dictionary
SAMPLE_METADATA = {
    "album_artist": "Pity Sex",
    "album_title": "White Hot Moon",
    "track_number": "06",
    "track_title": "Plum",
}

# A sample list of encoders
SAMPLE_ENCODERS = SAMPLE_CONFIGURATION["convert"]["encoder"]

# Some input lists for parametrized tests
ENCODER_NAME_INPUTS = [
    (SAMPLE_ENCODERS[0], "FLAC"),
    (SAMPLE_ENCODERS[1], "MP3_320"),
]

ENCODER_OPTIONS_VALUES = [
    (SAMPLE_ENCODERS[0], ["-c:a", "flac", "-compression_level", "8"]),
    (SAMPLE_ENCODERS[1], ["-c:a", "libmp3lame", "-b:a", "320k", "-q:a", "3"]),
]

GET_DICT_VALUE_PARAMS = [
    (("test_list", 0), "result"),
    (("test_dict", 1), ''),
]

REMOVE_SPECIAL_PARAMS = [
    ("Love is Love//Return To Dust", "Love is Love--Return To Dust"),
    ("Chunk, No Captain Chunk!", "Chunk, No Captain Chunk-"),
]


@pytest.mark.parametrize("raw_string,serialized_string", REMOVE_SPECIAL_PARAMS)
def test_remove_special(raw_string, serialized_string):
    assert mousiki.util.remove_special(raw_string) == serialized_string


def test_command_object():
    result = mousiki.util.get_ffmpeg_command(
        "/path/to/file.flac",
        SAMPLE_ENCODERS,
        ["flac/file.flac", "mpeg/file.mp3"]
    )

    expected = [
                "ffmpeg", "-loglevel", "error", "-y",
                "-i", "/path/to/file.flac",
                "-c:a", "flac", "-compression_level", "8", "flac/file.flac",
                "-c:a", "libmp3lame", "-b:a", "320k", "-q:a", "3", "mpeg/file.mp3",
               ]

    assert result == expected


@pytest.mark.parametrize("index_tuple,result", GET_DICT_VALUE_PARAMS)
def test_get_dict_value(index_tuple, result):
    a_dict = {
        "test_list": ["result"]
    }
    assert mousiki.util.get_dict_value(a_dict, *index_tuple, default='') == result

def test_output_paths():

    expected_before_format = [
        "/home/tmose1106/Music/Convert/FLAC/{album_artist}/{album_title}/{track_number}-{track_title}.flac",
        "/home/tmose1106/Music/Convert/MP3_320/{album_artist}/{album_title}/{track_number}-{track_title}.mp3",
    ]

    expected_after_format = [
        "/home/tmose1106/Music/Convert/FLAC/Pity Sex/White Hot Moon/06-Plum.flac",
        "/home/tmose1106/Music/Convert/MP3_320/Pity Sex/White Hot Moon/06-Plum.mp3",
    ]

    raw_result = list(mousiki.util.get_output_metapaths(
                        SAMPLE_CONFIGURATION,
                        SAMPLE_CONFIGURATION["convert"]
                      )
                     )


    formatted_result = [a_string.format(**SAMPLE_METADATA) for a_string in raw_result]

    for expected_str, result_str in zip(expected_before_format, raw_result):
        assert expected_str == result_str

    for expected_str, result_str in zip(expected_after_format, formatted_result):
        assert expected_str == result_str


@pytest.mark.parametrize("encoder_dict,expected", ENCODER_NAME_INPUTS)
def test_encoder_name(encoder_dict, expected):
    result = mousiki.util.get_encoder_name(encoder_dict)
    assert result == expected


@pytest.mark.parametrize("encoder,expected", ENCODER_OPTIONS_VALUES)
def test_encoder_options(encoder, expected):
    result = mousiki.util.get_encoder_options(encoder)

    assert expected == result

def test_update_dict_recursively():
    original_dict = {
        'Name': "Tad",
        'Age': 18,
        'Hobbies': {
            'Running': True,
        },
    }

    update_dict = {
        'Name': "Tad",
        'Age': 19,
        'Hobbies': {
            'Studying': True,
        },
    }

    expected_dict = {
        'Name': "Tad",
        'Age': 19,
        'Hobbies': {
            'Running': True,
        },
    }

    result_dict = mousiki.util.update_dict_recursively(original_dict, update_dict)

    assert utils.compare_dictionaries(result_dict, expected_dict)
