"""
Miscellaneous utilities for internal use within mousiki.
"""

# Standard
import collections
import copy
import os
import pathlib
# Dependency
import natsort


def _remove_characters(a_string, replacement_set, replacement_char):
    def removal_generator():
        for char in a_string:
            if char not in replacement_set:
                yield char
            else:
                yield replacement_char

    return ''.join(list(removal_generator()))


def remove_special(a_string, replacement_char='-'):
    """
    Remove special characters from a string, and replace them with
    *replacement_char*.
    """
    removal_characters = {
        '<', '>', ':', '\'', '"', '/', '\\', '|', '?', '!', '*'
    }
    return _remove_characters(a_string, removal_characters, replacement_char)


def serialize_file_name(file_name_str):
    """ Standardize the characters contained within a file's name. """
    return remove_special(file_name_str).replace(' ', '-').lower()


def get_ffmpeg_command(input_file, encoder_list, output_paths):
    command_list = ["ffmpeg",
                    # Do not display message unless they are errors
                    "-loglevel", "error",
                    # Overwrite output files by default
                    "-y",
                    # Set the input flag for later use
                    "-i", input_file,
                    ]

    for encoder, output_path in zip(encoder_list, output_paths):
        command_list.extend([*get_encoder_options(encoder), output_path])

    return command_list


def _natural_sort_dict(a_dict):
    """ Return an ordered dictionary with naturally sorted keys. """
    out_dict = collections.OrderedDict()
    for key in natsort.natsorted(a_dict.keys()):
        out_dict[key] = a_dict[key]
    return out_dict


def get_parent_directories(path_object):
    """ Iterate over parent directories of a path. """
    path_object_parent_dir_names = path_object.parent.parts
    for index, parent_dir_name in enumerate(path_object_parent_dir_names):
        yield pathlib.Path(*path_object_parent_dir_names[0:(index + 1)])


def create_parent_directories(path_object):
    """ Create parent directories if they do not already exist. """
    for parent_path_object in get_parent_directories(path_object):
        if not parent_path_object.is_dir():
            parent_path_object.mkdir()


def make_paths_exist(*pathlib_objects):
    for path_object in pathlib_objects:
        if path_object.parent.is_dir():
            continue
        else:
            create_parent_directories(path_object)


def format_using_metadata(a_string, metadata_dict):
    """ Format a string using a metadata dictionary. """
    return a_string.format(**metadata_dict)


def format_using_environment(a_string):
    """
    Format a string using a dictionary of environment variables found
    by **os.environ**.
    """
    return a_string.format(**os.environ)


def format_using_metaenv(a_string, metadata_dict):
    """
    Format a string (representing a path) using metadata and
    environment variables.
    """
    serialized_metadata_dict = {key: remove_special(value)
                                for key, value in metadata_dict.items()}
    return a_string.format(**os.environ, **serialized_metadata_dict)


def get_metaenv_path(path_object, metadata_dict):
    return pathlib.Path(format_using_metaenv(str(path_object), metadata_dict))


def get_encoder_extension(encoder):
    extension_dict = {
        "flac": "flac",
        "mp3": "mp3",
        "oggvorbis": "ogg",
    }

    try:
        return extension_dict[encoder["codec"]]
    except KeyError:
        return None


def get_dict_value(a_dict, *indexes, default=None):
    """
    Retrieve a value within a dictionary by following the provided
    indexes. If an index does no exist, *default* is returned.
    """
    value = a_dict
    for index in indexes:
        try:
            value = value[index]
        except (KeyError, IndexError):
            return default
    return value


def get_encoder_options(encoder):

    def get_kbps_string(value):
        """
        Return a string representing kilobytes per second of an audio
        stream.
        """
        return "{}k".format(value)

    default_encoders = {
        "flac": {
            "compression_level": 5,
            "output_dir": "FLAC/{album_artist}/{album_title}",
        },
        "mp3": {
            "bitrate": 128,
            "compression_level": 3,
            "output_dir": "MP3_128/{album_artist}/{album_title}",
        },
    }

    encoding_option_mappings = {
        "flac": {
            "compression_level": ("-compression_level", None),
        },
        "mp3": {
            "bitrate": ("-b:a", get_kbps_string),
            "compression_level": ("-q:a", None),
        },
    }

    codec_names = {
        "flac": "flac",
        "mp3": "libmp3lame",
        "ogg": "libvorbis",
    }
    encoder_options = []
    codec = encoder["codec"]

    try:
        encoder_options.extend(("-c:a", codec_names[codec]))
    except KeyError:
        print("Codec \'{}\' does not exist!")
        return None

    for key, value in _natural_sort_dict(encoder).items():
        # Skip over keys not related to setting options
        if key in ("codec", "output_dir"):
            continue

        if key not in encoding_option_mappings[codec]:
            continue

        cli_option, value_func = encoding_option_mappings[codec][key]

        if value_func:
            cli_value = value_func(value)
        else:
            cli_value = str(value)

        encoder_options.extend((cli_option, cli_value))

    return encoder_options


def get_encoder_name(encoder_dict):
    """ Generate a name for the encoder. """
    encoder_name = encoder_dict["codec"].upper()

    if "bitrate" in encoder_dict:
        encoder_name = "{}_{}".format(encoder_name, str(encoder_dict["bitrate"]))

    return encoder_name


def get_output_metapaths(global_config_dict, command_config_dict):
    root_path = global_config_dict["output_dir"]
    command_path = command_config_dict["output_dir"]
    path_root = pathlib.Path(root_path, command_path)

    file_name_scheme = global_config_dict["file_name_scheme"]

    for encoder in command_config_dict["encoder"]:
        encoder_path = encoder["output_dir"]
        file_name_placeholder = '.'.join((file_name_scheme,
                                         get_encoder_extension(encoder)))

        yield str(path_root.joinpath(encoder_path, file_name_placeholder))


def get_pathlib_objects(*paths):
    for path_string in paths:
        yield pathlib.Path(path_string)


def iterate_directory_recursively(path_object, max_depth=-1, _depth=0):
    """ Iterate over filenames recursively within a specified path. """
    # Determine if the maximum depth was set correctly
    max_depth_set = bool(max_depth <= -1)
    for item in os.scandir(path_object):
        # See if we have searched too deep or not
        too_deep = bool(_depth == max_depth)
        if item.is_dir() and (max_depth_set != too_deep):
            yield from iterate_directory_recursively(item.path,
                                                     max_depth=max_depth,
                                                     _depth=(_depth + 1)
                                                     )
        elif item.is_file():
            yield pathlib.Path(item.path)


def iterate_paths_recursively(*path_strings, max_depth=-1):
    """
    Iterate over paths (file or directory) yielding all file paths
    present.
    """

    pathlib_objects = get_pathlib_objects(*path_strings)

    for path_object in pathlib_objects:
        if path_object.is_file():
            if path_object.is_absolute():
                yield path_object
            else:
                yield pathlib.Path(os.path.abspath(path_object))
        elif path_object.is_dir():
            yield from iterate_directory_recursively(path_object)


def ask_user(question, choices, default=None):
    if default is not None:
        assert default in choices, "default value must be in the list of choices"

    choices_string = '/'.join(
        choice.upper() if choice == default else choice
        for choice in choices
    )

    prompt_string = "{} [{}] ".format(question, choice_string)
    while True:
        response = input(prompt_string).lower()
        if default is not None and response == '':
            return default
        elif response in choices:
            return response


def user_confirm(question, default=None):
    return ask(question, choices=['y', 'n'], default=default) == 'y'


def update_dict_recursively(original_dict, update_dict):
    """
    Update existing keys found within *original_dict* using the
    cooresponding value found within *update_dict*. If *add_key*
    is True, then keys found within *update_dict* but not within
    *original_dict* will be added as well.
    """
    copy_dict = copy.deepcopy(original_dict)
    for key, value in update_dict.items():
        if isinstance(value, collections.Mapping):
            copy_dict[key] = update_dict_recursively(copy_dict.get(key, {}), value)
        elif key in copy_dict:
            copy_dict[key] = update_dict[key]

    return copy_dict
