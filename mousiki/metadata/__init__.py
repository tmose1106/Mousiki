"""
 A module for handling audio metadata using a standardized interface
for a series of popular formats.
"""

# Standard
import os
# Project
from mousiki.metadata import formats

FILE_FORMATS = {
    ".flac": formats.FLACMetadata,
    ".mp3": formats.MP3Metadata,
    ".mp4": formats.MP4Metadata,
    ".ogg": formats.OggVorbisMetadata,
}


def get_metadata_object(a_path):
    """"""
    _, file_extension = os.path.splitext(a_path)
    if file_extension in FILE_FORMATS:
        return FILE_FORMATS[file_extension](str(a_path))
    else:
        return None


def get_metadata_dict(a_path):
    """
    """
    metadata_object = get_metadata_object(a_path)
    return metadata_object.as_dict() if metadata_object else None


def write_metadata_dict(a_path, a_dict):
    """
    """
    metadata_obj = get_metadata_object(a_path)
    metadata_obj.update(a_dict)
    metadata_obj.write()
