"""
Individual classes for metadata formats.
"""

# Standard
import base64
# Dependency
import mutagen.flac
import mutagen.oggvorbis
import mutagen.id3
import mutagen.mp4
# Project
from mousiki.metadata import base


class MP3Metadata(base.MetadataBase):
    """
    """

    _loader = mutagen.id3.ID3
    _loader_error = mutagen.id3._util.ID3NoHeaderError

    _key_precedence = [
        # album_artist
        ("TPE2",),
        # artist_name
        ("TPE1",),
        # album_title
        ("TALB",),
        # disc_number
        ("TPOS",),
        # genre
        ("TCON",),
        # release_date
        ("TDRC", "TDRL", "TXXX:Year",),
        # release_label
        ("TPUB",),
        # track_number
        ("TRCK",),
        # track_title
        ("TIT2",),
        # lyrics_unsynced
        ("USLT",),
        # comment
        ("COMM",),
    ]

    _key_handlers = {
        "TALB": (("album_title",), None, None),
        "TCON": (("genre",), None, None),
        "TDRC": (("release_date",), "_read_date_tag", None),
        "TDRL": (("release_date",), "_read_date_tag", None),
        "TIT2": (("track_title",), None, None),
        "TPE1": (("artist_name",), None, None),
        "TPE2": (("album_artist",), None, None),
        "TPOS": (("disc_number", "disc_total"), "_read_slash_tag", "_format_slash_tag"),
        "TPUB": (("release_label",), None, None),
        "TRCK": (("track_number", "track_total"), "_read_slash_tag", "_format_slash_tag"),
        # "TXXX:Year": (("released_date",), "_read_date_tag", None),
        "USLT": (("lyrics_unsynced",), None, None),
        "COMM": (("comment",), None, None),
    }

    def _read_tag(self, meta_key):
        return self.file[meta_key].text[0]

    def _read_slash_tag(self, meta_key):
        tag_value = self._read_tag(meta_key)
        if '/' in tag_value:
            return tuple(tag_value.split('/'))
        else:
            return tag_value, ''

    def _format_slash_tag(self, tmp_key_1, tmp_key_2):
        value_1 = self[tmp_key_1]
        value_2 = self[tmp_key_2]
        if value_2:
            return '/'.join((value_1.zfill(2), value_2.zfill(2)))
        else:
            return value_1

    def _write_tag(self, meta_key, value):
        self.file[meta_key] = getattr(mutagen.id3, meta_key)(encoding=3, text=str(value))

    def get_art(self):
        try:
            return self.file.getall("APIC")[0].data
        except KeyError:
            print("Couldn't load album art.")

    def write_art(self, art_data, mime_type="image/jpeg", description="Front Cover"):
        tag_obj = mutagen.id3.APIC(encoding=3,
                                   mime=mime_type,
                                   desc=description,
                                   data=art_data)
        self.file.add(tag_obj)


class FLACMetadata(base.MetadataBase):
    """ Class for dealing with FLAC metadata. """

    _loader = mutagen.flac.FLAC

    _key_precedence = [
        # album_artist
        ("ALBUMARTIST",),
        # artist_name
        ("ARTIST",),
        # album_title
        ("ALBUM",),
        # disc_number
        ("DISCNUMBER",),
        # disc_total
        ("DISCTOTAL", "TOTALDISCS"),
        # genre
        ("GENRE",),
        # release_date
        ("DATE",),
        # release_label
        ("LABEL",),
        # track_number
        ("TRACKNUMBER",),
        # track_total
        ("TRACKTOTAL", "TOTALTRACKS"),
        # track_title
        ("TITLE",),
        # lyrics_unsynced
        ("UNSYNCEDLYRICS",),
        # comment
        ("COMMENT", "COMMENTS",),
    ]

    _key_handlers = {
        "ALBUM": (("album_title",), None, None),
        "ALBUMARTIST": (("album_artist",), None, None),
        "ARTIST": (("artist_name",), None, None),
        "DATE": (("release_date",), "_read_date_tag", None),
        "DISCTOTAL": (("disc_total",), None, "_format_padded_tag"),
        "LABEL": (("release_label",), None, None),
        "GENRE": (("genre",), None, None),
        "TITLE": (("track_title",), None, None),
        "TRACKNUMBER": (("track_number",), None, "_format_padded_tag"),
        "TRACKTOTAL": (("track_total",), None, "_format_padded_tag"),
        "TOTALTRACKS": (("track_total",), None, "_format_padded_tag"),
        "DISCNUMBER": (("disc_number",), None, "_format_padded_tag"),
        "TOTALDISCS": (("disc_total",), None, "_format_padded_tag"),
        "UNSYNCEDLYRICS": (("lyrics_unsynced",), None, None),
        "COMMENT": (("comment",), None, None),
    }

    def get_art(self):
        try:
            return self.file.pictures[0].data
        except KeyError:
            print("Couldn't load album art.")

    def get_pic_obj(self, art_data, mime_type="image/jpeg", pic_type=3):
        pic_obj = mutagen.flac.Picture()
        pic_obj.data = art_data
        pic_obj.mime = mime_type
        pic_obj.type = pic_type

        return pic_obj

    def write_art(self, art_data):
        pic_obj = self.get_pic_obj(art_data)
        self.file.add_picture(pic_obj)


class OggVorbisMetadata(FLACMetadata):

    _loader = mutagen.oggvorbis.OggVorbis

    def get_art(self):
        return base64.b64decode(self.file["METADATA_BLOCK_PICTURE"])

    def write_art(self, art_data):
        pic_obj = self.get_pic_obj(art_data)
        self.file["METADATA_BLOCK_PICTURE"] = str(base64.b64encode(pic_obj.write()), "utf-8")


class MP4Metadata(base.MetadataBase):
    """ Class for dealing with MP4 metadata. """

    _loader = mutagen.mp4.MP4

    _key_precedence = {
        ("\xa9alb",),
        ("aART",),
        ("\xa9ART",),
        ("\xa9day",),
        ("LABEL",),
        ("\xa9gen",),
        ("\xa9nam",),
        ("trkn",),
        ("disk",),
    }

    _key_handlers = {
        ("\xa9alb",): (("album_title",), None, None),
        ("aART",): (("album_artist",), None, None),
        ("\xa9ART",): (("artist_name",), None, None),
        ("\xa9day",): (("release_date",), "_read_date_tag", None),
        ("LABEL",): (("release_label",), None, None),
        ("\xa9gen",): (("genre",), None, None),
        ("\xa9nam",): (("track_title",), None, None),
        ("trkn",): (("track_number", "track_total",), None, None),
        ("disk",): (("disc_number", "disc_total",), None, None),
    }
