"""
A base class for metadata formats.
"""

# Dependency
import click


class MetadataBase():
    """
    A base class for representing metadata, with helper functions for
    reading, retriving and writing new metadata fields.
    """

    _tmp_dict = {}

    def __init__(self, a_file):
        try:
            self.file = self._loader(a_file)
        except self._loader_error:
            raise RuntimeError("Metadata tag not loadable!")

        self.read()

    def __getitem__(self, key):
        return self._tmp_dict[key]

    def __setitem__(self, key, value):
        self._tmp_dict[key] = value

    def _format_tag(self, tmp_key):
        """ Format what will be written by self.write() """
        return self[tmp_key]

    def _format_padded_tag(self, tmp_key):
        return str(self[tmp_key]).zfill(2)

    def _read_tag(self, meta_key):
        return self.file[meta_key][0]

    def _read_date_tag(self, meta_key):
        date_value = str(self._read_tag(meta_key))
        if len(date_value) > 4:
            return date_value[:4]
        else:
            return date_value

    def _format_write_value(self, meta_key):
        handler = self._key_handlers[meta_key]
        format_func_name = handler[2]
        format_func = getattr(self, format_func_name) if format_func_name else self._format_tag
        value = format_func(*handler[0])
        return value if value else None

    def _write_tag(self, meta_key, value):
        self.file[meta_key] = value

    def as_dict(self):
        """ Return the temporary metadata dictionary. """
        return self._tmp_dict

    def read(self):
        """
        Read tags from the Mutagen object and apply them to the
        internal temporary metadata dictionary. Also useful for
        reseting values.
        """
        for precedence_tuple in self._key_precedence:
            for meta_key in precedence_tuple:
                if meta_key not in self.file:
                    continue
                tmp_keys, read_func_name, _ = self._key_handlers[meta_key]
                read_func = getattr(self, read_func_name) if read_func_name else self._read_tag
                break

            try:
                read_values = read_func(meta_key)
            except UnboundLocalError:
                continue
            except KeyError:
                continue
            if type(read_values) != tuple:
                read_values = tuple([read_values])
            for tmp_key, value in zip(tmp_keys, read_values):
                self._tmp_dict[tmp_key] = value

    def update(self, tmp_dict):
        """ Accept a dictionary of new tags as new values. """
        self._tmp_dict.update(tmp_dict)

    def write(self):
        """ Write temporary tag values back to the file. """
        for key_tuple in self._key_precedence:
            meta_key = key_tuple[0]
            try:
                write_value = self._format_write_value(meta_key)
                self._write_tag(meta_key, write_value)
            except KeyError:
                print(click.style("Warning:", fg="red"),
                      "No value for {}. Skipping.".format(meta_key)
                     )
                continue
        # Allow Mutagen to write the new tags to the file
        self.file.save()

    def write_art(self, *args, **kwargs):
        click.echo("write_art function not defined for \"{}\"!".format(self.__name__), err=True)
