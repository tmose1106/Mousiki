"""
A module for handling metadata from a compact disc.
"""

# Standard
import copy
import sys
# Dependencies
import click
import discid
import musicbrainzngs
# Project
import mousiki
from mousiki import util


class CDMetadata():
    """
    A class for handling CD metadata, wrapping musicbrainzngs output
    and returning it in the format of a mousiki temporary dictionary.
    """
    _album_metadata = {}
    _track_data = []

    def __init__(self):
        self._email = "tmoseley1106@gmail.com"

        disc_id, disc_submission_url = self._get_disc_id()
        self.raw_metadata = self._get_musicbrainz_metadata(disc_id,
                                                           disc_submission_url)
        self._parse_raw_metadata()

    def _get_disc_id(self):
        try:
            # Read the disc
            disc_read = discid.read()
            # Get the disc ID from the read object
            disc_id = disc_read.id
            # Also get the submission URL in case of an accident
            disc_submission = disc_read.submission_url
        except discid.disc.DiscError:
            click.echo("Please insert a compact disc, please.", err=True)
            sys.exit(1)

        return disc_id, disc_submission

    def _get_musicbrainz_metadata(self, disc_id, disc_url):
        musicbrainzngs.set_useragent("mousiki Cd Rip",
                                     mousiki.__version__,
                                     self._email)
        try:
            includes_list = ["artists", "artist-credits", "labels", "recordings"]
            return musicbrainzngs.get_releases_by_discid(disc_id,
                                                         includes=includes_list)
        except musicbrainzngs.ResponseError:
            click.echo("Disc was not found in Musicbrainz Database")
            click.echo("Please follow the link to manually handle the issue")
            click.echo(disc_url)
            sys.exit(1)
        except musicbrainzngs.musicbrainz.NetworkError:
            click.echo("Could not connect to Musicbrainz server")
            sys.exit(1)

    def _parse_raw_metadata(self):
        release_dict = self.raw_metadata['disc']['release-list'][0]

        def get_release_dict_value(*indexes):
            return util.get_dict_value(release_dict, *indexes, default='')

        album_info_indexes = {
            "album_title": ("title",),
            "album_artist": ("artist-credit", 0, "artist", "name",),
            "artist": ("artist-credit", 0, "artist", "name",),
            "release_label": ("label-info-list", 0, "label", "name",),
        }

        for tmp_key, index_tuple in album_info_indexes.items():
            self.set_tag(tmp_key, get_release_dict_value(*index_tuple))

        # Handle the release date manually
        date_value = get_release_dict_value("date")
        if len(date_value) > 4:
            date_value = date_value[:4]
        self.set_tag("release_date", date_value)

        # Handle the track total count manually as well
        disc_count = get_release_dict_value("medium-count")

        if disc_count > 1:
            disc_values = list(range(disc_count))
            click.echo("The current disc is part of a set")
            click.echo("Please choose a disc number from [{}]".format(disc_values))
            while True:
                try:
                    disc_number = int(input()) - 1
                    if disc_count in disc_values:
                        break
                except ValueError:
                    click.echo("Invalid response entered!")
                    continue
        else:
            disc_number = 0

        self.set_tag("disc_number", str(disc_number + 1).zfill(2))
        self.set_tag("disc_total", str(disc_count).zfill(2))

        track_total_value = get_release_dict_value("medium-list",
                                                   disc_number,
                                                   "track-count"
                                                   )
        self.set_tag("track_total", str(track_total_value))

        for track in get_release_dict_value("medium-list", disc_number, "track-list"):
            track_title = util.get_dict_value(track, "recording", "title")
            track_artist = util.get_dict_value(track, "recording", "artist-credit", 0, "artist", "name")
            self._track_data.append((track_title, track_artist))

    def get_track_metadata(self, track_index):
        """ Get the full metadata for a track using it's index. """
        track_dict = copy.deepcopy(self._album_metadata)
        track_dict["track_number"] = str(track_index + 1).zfill(2)
        track_dict["track_title"], track_dict["artist_name"] = self._track_data[track_index]

        return track_dict

    def get_track_iterator(self):
        """ Return a generator which iterates over metadata for tracks. """
        for index in range(len(self._track_data)):
            yield self.get_track_metadata(index)

    def get_album_metadata(self):
        """ Return a metadata dictionary for the entire album. """
        return self._album_metadata

    def set_tag(self, tmp_key, value):
        """ Set an album metadata dictionary value. """
        self._album_metadata[tmp_key] = value
