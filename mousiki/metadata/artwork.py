"""
Tools for finding and handling album artwork.
"""
# Standard
import os
# Project
from mousiki import util


def find_local_art_path(config_dict, metadata_dict):
    """ Find album artwork locally stored in a directory. """
    def get_config_dict_value(*indexes):
        return util.get_dict_value(config_dict, *indexes)

    artwork_dir_template = get_config_dict_value("artwork_dir")
    if not artwork_dir_template:
        return None

    artwork_naming_template = get_config_dict_value("artwork_naming_scheme")
    if not artwork_naming_template:
        return None

    # Make sure the local art directory exists
    artwork_dir = util.format_using_environment(artwork_dir_template)
    if not os.path.isdir(artwork_dir):
        return None

    artwork_directory = config_dict['artwork']['resized_dir']
    artwork_naming_template = config_dict['artwork']['file_name_scheme']

    raw_artwork_file_name = util.format_using_metadata(artwork_naming_template,
                                                       metadata_dict)

    serialized_artwork_file_name = util.serialize_file_name(raw_artwork_file_name)

    artwork_file_path = util.join_path_safely(*artwork_directory, serialized_artwork_file_name)

    if not os.path.isfile(artwork_file_path):
        return None

    return artwork_file_path


def get_file_art(metadata_object):
    return metadata_object.get_art()


def get_local_art(config_dict, metadata_dict):
    art_file_path = find_local_art_path(config_dict, metadata_dict)

    if art_file_path:
        with open(art_file_path, "rb") as art_file_object:
            return art_file_object.read()
    else:
        return None


def get_online_art():
    pass

def get_album_art():
    pass
