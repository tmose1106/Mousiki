"""
Commands for maintaining and observing one's music library.
"""

# Standard
import math
import os
import pprint
# Dependency
import click
# Project
from mousiki import metadata, util


def _convert_byte_size(size_bytes):
    if size_bytes == 0:
        return "0 B"

    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)

    return "{} {}".format(s, size_name[i])


def _get_file_extension(a_path):
    _, file_extension = os.path.splitext(a_path)
    return file_extension if file_extension else "N/A"


@click.group()
def main():
    """ Analyze library statistics """
    pass


@click.command()
def find_duplicates():
    """ Attempt to find duplicate files within a library. """
    pass


@click.command()
@click.argument("extension")
@click.argument("path", type=click.Path(exists=True))
def find_filetype(extension, path):
    """ Find each file of a specific filetype """
    for path_str in util.iterate_directory_recursively(path):
        file_extension = _get_file_extension(path_str)
        if file_extension == extension:
            click.echo(os.path.relpath(path_str, path))


@click.command()
@click.argument("path", type=click.Path(exists=True))
def list_filetypes(path):
    """
    Find all of the different filetypes present within a library.
    """
    file_types = {}

    for path_str in util.iterate_directory_recursively(path):
        file_extension = _get_file_extension(path_str)
        file_size = os.path.getsize(path_str)
        try:
            file_types[file_extension][0] += 1
            file_types[file_extension][1] += file_size
        except KeyError:
            file_types[file_extension] = [1, file_size]
            table_lines = []

    for extension, data_tuple in file_types.items():
        human_file_size = _convert_byte_size(data_tuple[1])
        table_lines.append((extension, data_tuple[0], human_file_size))

    for line_tuple in table_lines:
        print(*line_tuple)


@click.command()
def fix_filenames():
    """
    Standardize the filenames of all audio files with a library.
    """
    pass


@click.command()
@click.argument("path", type=click.Path(exists=True))
def get_statistics(path):
    """
    Retrieve statistics by observing each file's metadata and
    recording information, then printing in table output.
    """
    # Keys that should be tallied for every special
    tally_keys = [
        "album_artist",
        "album_title",
        "artist",
        "release_date",
    ]

    tally_keys_dict = {key: set() for key in tally_keys}

    album_count_keys = [
        "release_date",
        "disc_total",
        "track_total",
    ]

    album_keys_dict = {key: {} for key in album_count_keys}

    # Metadata keys that should be counted for each occurance of a key
    track_count_keys = [
        "release_date",
        "disc_total",
    ]

    track_keys_dict = {key: {} for key in track_count_keys}

    for path_str in util.iterate_directory_recursively(path):
        track_metadata = metadata.get_metadata_dict(path_str)

        if track_metadata is None:
            continue

        for key, value in track_metadata.items():
            if key in tally_keys:
                tally_keys_dict[key].add(value)
            if key in album_count_keys:
                album_title = track_metadata["album_title"]
                try:
                    album_keys_dict[key][value].add(album_title)
                except KeyError:
                    album_keys_dict[key][value] = {album_title}
            if key in track_count_keys:
                try:
                    track_keys_dict[key][value] += 1
                except KeyError:
                    track_keys_dict[key][value] = 1

    print("\nTallys:")
    pprint.pprint(tally_keys_dict)
    for key, value in tally_keys_dict.items():
        print(key, len(value))
    print("\nAlbum Counts:")
    pprint.pprint(album_keys_dict)
    for tmp_key, count_dict in album_keys_dict.items():
        print(tmp_key)
        for key, value in count_dict.items():
            print(key, len(value))
    print("\nTrack Counts:")
    pprint.pprint(track_keys_dict)
    for tmp_key, count_dict in track_keys_dict.items():
        print(tmp_key)
        for key, value in count_dict.items():
            print(key, value)


main.add_command(find_filetype, name="find-filetype")
main.add_command(list_filetypes, name="list-filetypes")
main.add_command(get_statistics, name="stats")
