"""
Command for converting audio files.
"""

# Standard
import os
import subprocess
# Dependency
import click
# Project
from mousiki import config, metadata, util

CREATED_STR = click.style("Created:", fg="green", bold=True)
ERROR_STR = click.style("Error:", fg="red")
CONVERTING_STR = click.style("Converting:", bold=True)


@click.command()
@click.argument("path", nargs=-1, type=click.Path(exists=True), required=True)
@click.option("-d", "--depth", default=-1,
              help="Maximum depth to recurse downwards")
@click.help_option("-h", "--help")
def main(path, depth):
    """ Convert a list of tracks into new formats """
    global_config_dict = config.get_global_config()
    command_config_dict = util.get_dict_value(global_config_dict, "command", "convert")

    output_templates = list(util.get_output_metapaths(global_config_dict,
                                                      command_config_dict
                                                     )
                           )

    for path_str in util.iterate_paths_recursively(*path):
        track_metadata_object = metadata.get_metadata_object(path_str)
        track_metadata_dict = track_metadata_object.as_dict()

        if not track_metadata_dict:
            print(ERROR_STR,
                  "Could not open file \"{}\". Skipping.".format(path_str)
                 )
            continue

        album_art_data = track_metadata_object.get_art()

        print(CONVERTING_STR, os.path.abspath(path_str))

        output_paths = [util.get_metaenv_path(path_template, track_metadata_dict)
                        for path_template in output_templates]

        util.make_paths_exist(*output_paths)

        # Generate the command to run, run it, and make sure it ran fine
        ffmpeg_command = util.get_ffmpeg_command(path_str, command_config_dict["encoder"], output_paths)
        ffmpeg_process = subprocess.run(ffmpeg_command)
        ffmpeg_process.check_returncode()

        for new_file_path_str in output_paths:
            metadata_object = metadata.get_metadata_object(new_file_path_str)
            metadata_object.file.delete()
            metadata_object.update(track_metadata_dict)

            if album_art_data:
                metadata_object.write_art(album_art_data)

            metadata_object.write()
            print(CREATED_STR, new_file_path_str)
