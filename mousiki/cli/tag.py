"""
Commands for viewing and manipulating tags.
"""
# Dependency
import click
# Project
from mousiki import metadata


@click.command()
@click.argument("filepath", type=click.Path(exists=True))
def view(filepath):
    """ Print out the metadata of an audio file """
    tag_order = (
        "track_title",
        "artist_name",
        "album_artist",
        "album_title",
        "release_date",
        "release_label",
        "genre",
        "track_number",
        "track_total",
        "disc_number",
        "disc_total",
    )

    metadata_dict = metadata.get_metadata_dict(filepath)

    for key in tag_order:
        try:
            tag_value = metadata_dict[key]
            # Generate a tag title from the temporary tag key
            title = "{}:".format(key.replace('_', ' ').title())
            styled_title = click.style(title, bold=True)
            click.echo("{key:<25}{value}".format(key=styled_title,
                                                 value=tag_value))
        except KeyError:
            continue


@click.group()
def main():
    """ View and manipulate tags """
    pass


main.add_command(view, name="view")
