# Dependency
import click
# Project
import mousiki
from mousiki.cli import art, cd, convert, library, tag

COMMANDS = {
    "artwork": art.main,
    "cd": cd.main,
    "convert": convert.main,
    "library": library.main,
    "tag": tag.main,
}


@click.group()
@click.help_option("-h", "--help")
@click.version_option(mousiki.__version__, "-V", "--version")
def main():
    """ A music library utility in Python """
    pass


for command_name, command_obj in COMMANDS.items():
    main.add_command(command_obj, name=command_name)
