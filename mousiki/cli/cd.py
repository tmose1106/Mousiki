"""
Commands for handling compact audio discs.
"""
# Standard
import pprint
import subprocess
# Dependency
import click
# Project
from mousiki import config, metadata, util
from mousiki.metadata import cd


@click.command()
@click.option("-r", "--raw", "raw_output",
              is_flag=True,
              help="Print raw Musicbrainz response")
@click.help_option("-h", "--help")
def print_metadata(raw_output):
    """ Print metadata for current audio disc """
    disc_metadata_obj = cd.CDMetadata()

    if raw_output:
        pprint.pprint(disc_metadata_obj.raw_metadata)
        return

    for key, value in disc_metadata_obj.get_album_metadata().items():
        title = key.replace('_', ' ').title()
        styled_title = click.style(title, bold=True)
        spacer = ' ' * (17 - len(title))
        click.echo("{}:{}{}".format(styled_title, spacer, value))


@click.command()
@click.option("-n", "--track-numbers", default="",
              help="Defines list of tracks to be ripped")
@click.help_option("-h", "--help")
def rip_compact_disc(track_numbers):
    """
    Rip a compact disc and apply metadata
    """
    # Get configurations
    global_config_dict = config.get_global_config()
    command_config_dict = global_config_dict["command"]["cd"]["rip"]

    # Define which track numbers should be ripped
    track_include_list = [track_number for track_number in track_numbers.split(',')]
    track_include_list.remove('')
    # Get the disc's metadata
    disc_metadata_obj = cd.CDMetadata()

    output_templates = list(util.get_output_metapaths(global_config_dict, command_config_dict))

    for track_index, track_metadata_dict in enumerate(disc_metadata_obj.get_track_iterator()):
        track_number_str = str(track_index + 1)

        # Skip the track if it is not listed in the include list
        if track_include_list and track_number_str not in track_include_list:
            click.echo("Skipping track #{}".format(track_number_str))
            continue

        output_paths = [util.get_metaenv_path(path_template, track_metadata_dict)
                        for path_template in output_templates]
        # Make sure output directories exist
        util.make_paths_exist(*output_paths)
        cdparanoia_command_list = ["cdparanoia",
                                   # Quiet output
                                   "-q",
                                   # Specify a track number
                                   "-B", track_number_str,
                                   # Send the ripped data to stdout
                                   "-"]
        ffmpeg_command_list = util.get_ffmpeg_command("pipe:0",
                                                      command_config_dict['encoder'],
                                                      output_paths
                                                      )

        cdparanoia_command = subprocess.Popen(cdparanoia_command_list,
                                              stdout=subprocess.PIPE)
        ffmpeg_command = subprocess.run(ffmpeg_command_list,
                                        stdin=cdparanoia_command.stdout)
        # Ensure that the commands completed successfully
        ffmpeg_command.check_returncode()

        for new_file_path_str in output_paths:
            metadata.write_metadata_dict(new_file_path_str, track_metadata_dict)
            print(click.style("Created:", fg="green", bold=True), new_file_path_str)

    click.echo("All done, enjoy your music.")


@click.group()
def main():
    """ Dealing with compact discs. """
    pass


main.add_command(print_metadata, name="view")
main.add_command(rip_compact_disc, name="rip")
