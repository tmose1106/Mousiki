"""
Commands for viewing and manipulating album artwork tags.
"""
# Standard
import io
import os
import sys
# Dependency
import click
from PIL import Image
# Project
from mousiki import config, metadata, util


@click.command()
@click.argument("filepath", type=click.Path(exists=True))
def view_embedded_art(filepath):
    """ Display a file's embedded album artwork """
    meta_obj = metadata.get_metadata_object(filepath)
    art_filelike = io.BytesIO(meta_obj.get_art())
    art_image = Image.open(art_filelike)
    art_image.show()


@click.command()
@click.argument("file_path", required=True, type=click.Path(exists=True))
@click.argument("art_path", required=True, type=click.Path(exists=True))
# @click.option("-r", "--remove", help="Remove all other picture tags")
def write_art_file(file_path, art_path):
    """ Write an image to an audio file's embedded album artwork """
    metadata_obj = metadata.get_metadata_object(file_path)

    with open(art_path, "rb") as art_bytes:
        metadata_obj.write_art(art_bytes.read())
    metadata_obj.write_tags()


@click.command()
def batch_resize_artwork():
    """ Batch resize the specified artwork directories """
    global_config_dict = config.get_global_config()
    raw_art_dir_list = global_config_dict['artwork']['raw_dir']
    raw_art_dir_str = util.format_using_environment(util.join_path_safely(*raw_art_dir_list))
    # Exit if the raw artwork directory does not exist
    if not os.path.isdir(raw_art_dir_str):
        click.echo("Raw artwork directory at \'{}\' doesn't exist".format(raw_art_dir_str), err=True)
        sys.exit(1)
    resized_art_dir_list = global_config_dict['artwork']['resized_dir']
    resized_art_dir_str = util.format_using_environment(util.join_path_safely(*resized_art_dir_list))
    # Create the resized artwork directory if it doesn't exist
    util.create_directories(resized_art_dir_str)
    click.echo("Outputing resized art to \'{}\'".format(resized_art_dir_str), err=True)
    for file_path in util.iterate_directory_recursively(raw_art_dir_str, max_depth=0):
        new_file_name = util.serialize_file_name(os.path.basename(file_path))
        new_file_path = os.path.join(resized_art_dir_str, new_file_name)
        click.echo("{} -> {}".format(file_path, new_file_path))
        image_object = Image.open(file_path)
        # Define new dimensions for the resized image
        height, width = image_object.size
        new_width = global_config_dict['artwork']['size']
        new_height = int(new_width * (height / width))
        # Resize the image and save it to a new file path
        new_image_object = image_object.resize((new_width, new_height), Image.ANTIALIAS)
        new_image_object.save(new_file_path)


@click.group()
def main():
    """ View an audio file's album artwork """
    pass


main.add_command(batch_resize_artwork, name="resize")
main.add_command(view_embedded_art, name="view")
main.add_command(write_art_file, name="write")
