"""
Functions handling configuration of the project.
"""

# Standard
import os
# Dependencies
import click
import toml
import xdg.BaseDirectory as xdg
# Project
from mousiki import util


def _get_default_config():
    """ Generate a default configuration dictionary for mousiki. """
    if os.name == "posix":
        home_prefix = "{HOME}"

    return {
        # An absolute path to the root music output directory
        'output_dir': os.path.join(home_prefix, "Music"),
        # A string template for how to name audio files globally
        'file_name_scheme': "{track_number}-{track_title}",
        "artwork": {
            # A string template for how to name art files
            "file_name_scheme": "{album_artist}-{album_title}",
            # A path for raw artwork files
            "raw_dir": os.path.join(home_prefix, "Pictures", "Album Art", "Raw"),
            # A path for resized artwork files
            "resized_dir": os.path.join(home_prefix, "Pictures", "Album Art", "Resized"),
            # The default size for resized pictures (in pixels of width)
            "size": 500,
        },
        "command": {
            "cd": {
                # The ``mousiki cd rip`` command
                "rip": {
                    "output_dir": "Rip",
                    "encoder": [
                        {
                            "codec": "flac",
                            "compression": 8,
                            "output_dir": os.path.join("FLAC", "{album_artist}", "{album_title}"),
                        },
                        {
                            "codec": "mp3",
                            "bitrate": 128,
                            "output_dir": os.path.join("MP3_128", "{album_artist}", "{album_title}"),
                        },
                    ],
                },
            },
            # The ``mousiki convert`` command
            "convert": {
                "output_dir": "Convert",
                "encoder": [
                    {
                        "codec": "mp3",
                        "bitrate": 128,
                        "output_dir": os.path.join("MP3_128", "{album_artist}", "{album_title}"),
                    },
                    {
                        "codec": "mp3",
                        "bitrate": 320,
                        "output_dir": os.path.join("MP3_320", "{album_artist}", "{album_title}"),
                    },
                ],
            },
        },
    }


def _read_config(config_path):
    """
    Read a TOML configuration file in the proper directory defined by
    the XDG standard.
    """
    with open(config_path, 'r') as in_file:
        config_dict = toml.load(in_file)

    return config_dict


def _read_default_config():
    """
    Return the default configuration assuming that the standard
    configuration file (~/.config/mousiki/config.toml) exists.
    """
    config_path = os.path.join(xdg.load_first_config("mousiki"), "config.toml")

    if not os.path.isfile(config_path):
        write_default_config()

    return _read_config(config_path)


def get_global_config():
    """
    Get the configuration dictionary, minus all of the commands
    configuration options.
    """
    print(_read_default_config())
    return util.update_dict_recursively(_get_default_config(), _read_default_config())


def _write_config(output_path, config_dict):
    """
    Write a configuration dictionary to a TOML-formatted file in the
    proper configuration directory defined by the XDG standard.
    """
    with open(output_path, 'w') as out_file:
        toml.dump(config_dict, out_file)

    click.echo("Created config file at \"{}\"".format(output_path))


def write_default_config():
    """ Write the default configuration dictionary defined above. """
    print(_get_default_config())
    _write_config(os.path.join(xdg.load_first_config("mousiki"), "config.toml"), _get_default_config())
